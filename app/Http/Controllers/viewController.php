<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class viewController extends Controller
{
    //verPedidos
    public function modalOrderView(){
        return view('modal.pedidos.viewOrder');
    }
    //crearComission
    public function modalComissionNew(){
        return view('modal.comision.crearComision');
    }
    //crearClient
    public function modalClientNew(){
        return view('modal.clientes.crearCliente');
    }
    //editClient
    public function modalClientsEdit(){
        return view('modal.clientes.editar');
    }
    //deleteClient
    public function modalClientsDelete(){
        return view('modal.clientes.eliminar');
    }
    //viewCarriers
    public function modalCarriersView(){
        return view('modal.clientes.carriers');
    }
    //accessClient
    public function modalClientsReconfig(){
        return view('modal.clientes.reconfigClientes');
    }
    //authorizeClient
    public function modalClientsAuthorize(){
        return view('modal.clientes.authorizarClientes');
    }
    /* ADMIN */
}
