<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Autorizar</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div class="jumbotron text-center">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="card">
                        <div class="card mb-4 box-shadow p-3 mb-2 bg-info text-white">
                          
                            <p class="category"></p>
                        </div>
                        
                        <h1 class="jumbotron-heading">¡Nueva comision por autorizar!</h1>
                        {!! $name !!}
                        <!-- <form id="viewCarriers" method="GET" action="" class="form-horizontal" ng-controller="clientesController as cCtrl" ng-init="cCtrl.getCarriers(_aux)" enctype="multipart/form-data"> -->
                        <form id="viewCarriers" method="GET" action="{{ url('terminosCondiciones') }}" class="form-horizontal" enctype="multipart/form-data">
                        
                            <div class="modal-header">
                                <h4 class="modal-title"><i class="fa fa-user"></i>&nbsp; Carriers elegidos</h4>
                            </div>            

                                @foreach($carriers as $carr)
                                    <p>
                                        {!! $carr->name !!} - {!! $carr->comission !!}
                                    </p>
                                @endforeach
                                {!! $id !!}
                                
                            <div class="modal-footer">
                                <div class="row" ng-controller="clientesController">
                                    <div class="col-sm-12">
                                    
                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                        <input type="hidden" name="id" value="{!! $id !!}">
                                        <input type="hidden" name="email" value="{!! $email !!}">
                                        <input type="hidden" name="authorization" value="1">
                                        <!-- <button class="btn btn-warning btn-sm" ng-click="openAuthorizeClient()">
                                            <i class="btn btn-outline-danger rounded-circle"></i> Aceptar Autorizacion
                                        </button> -->
                                        <button type="submit" class="btn btn-primary">Aceptar autorizacion</button>
                                 
                                    </div>
                                </div>
                            </div>
                            <!--/.modal-footer -->
                        </form>
                        <!-- jQuery Setting Plugins -->
                        <!-- <script src="{{asset('/js/setting.plugins.js')}}"></script> -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
</body>
</html>