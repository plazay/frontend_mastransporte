<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
// use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Order extends Model
{
    use Notifiable;
    // protected $connection = 'mongodb';
    // protected $collection = 'order_data';
    protected $table = 'order_data';
		/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_client', 'id_store', 'num_order', 'date', 'id_carrier','product', 'service', 'destination_commune', 'weight', 'addressee', 'street', 'number', 'add-on_address',
        'reference', 'louder', 'wide', 'length', 'position_company', 'code_amount_collected', 'declared_value_product', 'email', 'cell', 'office_delivery', 'aditional_info',
        'add_agrupada, add_total_pieces', 'add_piece_number', 'company', 'addressee_secondary', 'declared_content_product', 'extended_coverage'
    ];
}