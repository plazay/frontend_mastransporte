<form id="deleteClient" method="POST" action="{{ url('clientDelete') }}" class="form-horizontal" ng-controller="clientesController" enctype="multipart/form-data">
  <!-- <input name="_method" type="hidden" value="PUT"/> -->
    <div class="modal-header">
        <h4 class="modal-title"><i class="fa fa-warning"></i>&nbsp; Eliminar Cliente</h4>
        @if (Session::has('message'))
          <p class="alert alert-success">{{ Session::get('message')}}</p>
        @endif
        <button type="button" class="close" ng-click="cancel()"><i class="fa fa-times"></i></button>
    </div>
          <div class="col-md-12">
            <div class="text-center">
              <h3 class="jumbotron-heading">Seguro que desea eliminar este cliente?</h3>
            </div>
          </div>
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-sm-12">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="hidden" name="id" value="[[_aux.id]]">
                <button type="submit" class="btn btn-danger">Eliminar</button>
                <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
            </div>
        </div>
    </div>
  </form>
<!-- jQuery Setting Plugins -->
<!-- <script src="{{asset('/js/setting.plugins.js')}}"></script> -->
    <!--/.modal-footer -->