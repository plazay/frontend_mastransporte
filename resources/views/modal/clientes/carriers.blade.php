
<form id="viewCarriers" method="GET" action="{{ url('emailAuthorization') }}" class="form-horizontal" ng-controller="clientesController as cCtrl" ng-init="cCtrl.getCarriers(_aux)" enctype="multipart/form-data">
  <!-- <input name="_method" type="hidden" value="PUT"/> -->
    <div class="modal-header">
        <h4 class="modal-title"><i class="fa fa-user"></i>&nbsp; Carriers elegidos</h4>
        <button type="button" class="close" ng-click="cancel()"><i class="fa fa-times"></i></button>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-sm-6" ng-repeat="carrier in cCtrl.Carriers">
                <div class="form-group">
                    <label class="col-sm-12">[[carrier.name]]</label>
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-text"><i class="fa fa-usd"></i></span>
                            <!-- <input type="text" name="name" class="form-control required" ng-model="_aux.name"> -->
                            <input type="text" name="[[carrier.name]]" class="form-control required" ng-model="carrier.comission">
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="col-sm-12 small">* Campos Obligatorios [[_aux.id]] - [[_aux.email]]</label>
                </div>
            </div>
        </div>
    </div>
    <!--/.modal-body -->
    <div class="modal-footer">
        <div class="row">
            <div class="col-sm-12">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="hidden" name="id" value="[[_aux.id]]">
                <input type="hidden" name="email" value="[[_aux.email]]">
                <button type="submit" class="btn btn-primary">Solicitar autorizacion</button>
                <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
            </div>
        </div>
    </div>
    <!--/.modal-footer -->
</form>
<!-- jQuery Setting Plugins -->
<!-- <script src="{{asset('/js/setting.plugins.js')}}"></script> -->