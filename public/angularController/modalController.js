//modal para ver pedidos
app.controller('ModalInstanceViewOrder',function($http,$scope,$modalInstance,items){
    $scope._aux = items;
    // $scope.selectedOrder = items.id;
    $scope.cancel = function () {
        $modalInstance().dismiss('cancel');
    };
});

//modal para crear comission
app.controller('ModalInstanceNewComission',function($http,$scope,$modalInstance,items){
    $scope.cancel = function () {
        $modalInstance().dismiss('cancel');
    };
});

//modal para crear comission
app.controller('ModalInstanceNewClient',function($http,$scope,$modalInstance,items){
    $scope.cancel = function () {
        $modalInstance().dismiss('cancel');
    };
});

//modal para editar cliente
app.controller('ModalInstanceEditClient',function($http,$scope,$modalInstance,items){

    $scope._aux = items;
    $scope.selectedBuyer = items.id;
       
    $scope.cancel = function () {
        $modalInstance().dismiss('cancel');
    };
});
//modal para eliminar cliente
app.controller('ModalInstanceDeleteClient',function($http,$scope,$modalInstance,items){

    $scope._aux = items;
    $scope.selectedBuyer = items.id;
       
    $scope.cancel = function () {
        $modalInstance().dismiss('cancel');
    };
});
//modal para ver carriers
app.controller('ModalInstanceViewCarriers',function($http,$scope,$modalInstance,items){

    $scope._aux = items;
    // console.log(items);
    // $scope.selectedBuyer = items.id;
    // console.log('$scope._aux: ',$scope._aux);
    $scope.cancel = function () {
        $modalInstance().dismiss('cancel');
    };
});
//Modal Activa / Desactiva
app.controller('ModalInstanceUpdateStatus',function($http, $scope, $modalInstance, items){
	$scope._aux = items;
	$scope.cancel = function () {
        $modalInstance().dismiss('cancel');
    };
});
//modal para dar acceso al cliente
app.controller('ModalInstanceReconfigClient',function($http,$scope,$modalInstance,items){

    $scope._aux = items;
    $scope.selectedBuyer = items.id;
       
    $scope.cancel = function () {
        $modalInstance().dismiss('cancel');
    };
});