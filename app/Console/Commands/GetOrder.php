<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class GetOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Obtener pedidos de tiendas por dia';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orders = \App\Order::all();
        return response($orders);
    }
}
