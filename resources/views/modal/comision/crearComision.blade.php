<form id="newForm" method="get" action="{{ url('newComission') }}" class="form-horizontal" ng-controller="comissionController" enctype="multipart/form-data">
  <!-- <input name="_method" type="hidden" value="PUT"/> -->
    <div class="modal-header">
        <h4 class="modal-title"><i class="fa fa-percent"></i>&nbsp; Crear Comision</h4>
        <button type="button" class="close" ng-click="cancel()"><i class="fa fa-times"></i></button>
    </div>
    <div class="modal-body" ng-controller="comissionController">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-12">Code*</label>
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-text"><i class="fa fa-creative-commons"></i></span>
                            <input type="text" name="code" class="form-control required" placeholder="Codigo Carriers">
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-12" for="nombre">Pais</label>
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-text"><i class="fa fa-globe"></i></span>
                            <input type="text" name="countries" class="form-control required" placeholder="Seleccione el Pais">
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-12">Carriers*</label>
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-text"><i class="fa fa-truck"></i></span>
                            <input type="text" name="carriers" class="form-control required" placeholder="Ingrese su Rut">
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-12">Comision*</label>
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-text"><i class="fa fa-percent" width="50" height="50"></i></span>
                            <input name="cost" class="form-control required" placeholder="Agregue la comision" type="text">

                            <!-- <select name="platform" ng-model="persona.medio" ng-options="obj.value as obj.label for obj in persona.canales" required>
                              <option value="">Platform</option>
                            </select> -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
        </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="col-sm-12 small">* Campos Obligatorios</label>
                </div>
            </div>
        </div>
    </div>
    <!--/.modal-body -->
    <div class="modal-footer">
        <div class="row">
            <div class="col-sm-12">
                <!-- <input type="hidden" name="monto_total" value="500">
                <input type="hidden" name="id" value="[[_aux.id]]"> -->
                <button type="submit" class="btn btn-primary">Crear</button>
                <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
            </div>
        </div>
    </div>
    <!--/.modal-footer -->
</form>
<!-- jQuery Setting Plugins -->
<!-- <script src="{{asset('/js/setting.plugins.js')}}"></script> -->