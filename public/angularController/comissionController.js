    app.controller("comissionController", function($scope, $http, $uibModal){
    $scope.loading = true;
    $scope.errorRut = false;
    $scope.showEmpresa = false;

    $scope.alertMessage = null;
         
    // Función que se encargará de mostrar el mensaje de alerta.
    $scope.openAlert = function showAlert() {
        $scope.alertMessage =   {
            // 'type' define el aspecto que tendrá el mensaje de alerta.
            type: "success",
            text: "El cliente ha sido modificado exitosamente",
            // Si 'closable' es 'true' se mostrará un botón para ocultar de manera manual el mensaje.
            closable: true,
            // número de segundos antes de que el mensaje de alerta desaparezca de forma automática.
            delay: 3
        };
    };

     //Modal Crear Comision
     $scope.openNewComission = function(data){
        $scope.items = data;
		var modalInstance = $uibModal.open({
			animation: true,
            backdrop  : 'static',
            keyboard  : false,
			templateUrl: '/modalNewComission',
			controller: 'ModalInstanceNewComission',
			resolve: {
				'$modalInstance': function() { return function() { return modalInstance; } },
				items: function () {
					return $scope.items;
				}
			}
		});
    };

    $scope.getSellersActive = function() {
        var resj = $http({
             method: 'GET',
             url: '/getSellersActive'
         })
         .success(function(data, status, headers, config){
             $scope.Sellers = data;
            
         })
         .error(function(data, status, headers, config){
             $scope.status = status;
         });
         return resj
     };

    //fin funciones aplicadas al B2B ------------------------------------------------------------------//

});