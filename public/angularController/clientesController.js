app.controller("clientesController", function($scope, $http, $uibModal){
    vm = this;
    $scope.loading = true;
    $scope.errorRut = false;
    $scope.showEmpresa = false;
    vm.client = {};
    vm.Carriers = [];

    vm.active = function() {
        // vm.getCarriers();
    }
    vm.active();

    $scope.alertMessage = null;

    vm.getCarriers = function(data) {
        // var resj = 
        // console.log(data.id);
        $http({
            method: 'GET',
            params: {id: data.id},
            url: '/ver/carriers'
        }).then(function (resp) {
            console.log('resp: ', resp);
            vm.Carriers = resp.data;
            console.log('vm.Carriers:', vm.Carriers);
        }).catch(function (err) {
            console.log('err: ', err);
        });
        // .success(function(data, status, headers, config){
        //     vm.Carriers = data;
        //     console.log('vm.Carriers:', vm.Carriers);
        // })
        // .error(function(data, status, headers, config){
        //     alert('hay que manejar el error');
        // });
        // return resj;
    };
         
    // Función que se encargará de mostrar el mensaje de alerta.
    vm.openAlert = function () {
        vm.alertMessage =   {
            // 'type' define el aspecto que tendrá el mensaje de alerta.
            type: "success",
            text: "El cliente ha sido modificado exitosamente",
            // Si 'closable' es 'true' se mostrará un botón para ocultar de manera manual el mensaje.
            closable: true,
            // número de segundos antes de que el mensaje de alerta desaparezca de forma automática.
            delay: 3
        };
    };

    //Modal Crear Client
    $scope.openNewClient = function(data){
        $scope.items = data;
        var modalInstance = $uibModal.open({
            animation: true,
            backdrop  : 'static',
            keyboard  : false,
            templateUrl: '/modalNewClient',
            controller: 'ModalInstanceNewComission',
            resolve: {
                '$modalInstance': function() { return function() { return modalInstance; } },
                items: function () {
                    return $scope.items;
                }
            }
        });
    };
     //Modal Editar Cliente
     $scope.openEditClient = function(data){
        //  console.log('data',data);
        $scope.items = data;
		var modalInstance = $uibModal.open({
			animation: true,
            backdrop  : 'static',
            keyboard  : false,
            size : 'md',
			templateUrl: '/modalClientsEdit',
			controller: 'ModalInstanceEditClient',
			resolve: {
				'$modalInstance': function() { return function() { return modalInstance; } },
				items: function () {
					return $scope.items;
				}
			}
		});
    };

    //Modal Eliminar Cliente
     $scope.openDeleteClient = function(data){
        //  console.log(data);
        $scope.items = data;
		var modalInstance = $uibModal.open({
			animation: true,
            backdrop  : 'static',
            keyboard  : false,
            size : 'md',
			templateUrl: '/modalClientsDelete',
			controller: 'ModalInstanceDeleteClient',
			resolve: {
				'$modalInstance': function() { return function() { return modalInstance; } },
				items: function () {
					return $scope.items;
				}
			}
		});
    };
//Modal Authorization client
    $scope.openAuthorizeClient = function(data){
        $scope.items = data;
		var modalInstance = $uibModal.open({
			animation: true,
            backdrop  : 'static',
            keyboard  : false,
			templateUrl: '/modalAutorizeClient',
			controller: 'ModalInstanceUpdateStatus',
			resolve: {
				'$modalInstance': function() { return function() { return modalInstance; } },
				items: function () {
					return $scope.items;
				}
			}
		});
    };
//Modal ver Carriers asignados
    $scope.openViewCarriers = function(data){
         console.log(data);
        $scope.items = data;
		var modalInstance = $uibModal.open({
			animation: true,
            backdrop  : 'static',
            keyboard  : false,
            size : 'md',
			templateUrl: '/modalCarriersView',
			controller: 'ModalInstanceViewCarriers',
			resolve: {
				'$modalInstance': function() { return function() { return modalInstance; } },
				items: function () {
					return $scope.items;
				}
			}
		});
    };
     //Modal Reconfigurar Cliente
     $scope.openReconfigClient = function(data){
        //  console.log(data);
        $scope.items = data;
		var modalInstance = $uibModal.open({
			animation: true,
            backdrop  : 'static',
            keyboard  : false,
            size : 'md',
			templateUrl: '/modalClientsReconfig',
			controller: 'ModalInstanceReconfigClient',
			resolve: {
				'$modalInstance': function() { return function() { return modalInstance; } },
				items: function () {
					return $scope.items;
				}
			}
		});
    };
     //Modal pedidos Cliente
     $scope.openViewOrderCHX = function(data){
         console.log(data);
        $scope.items = data;
		var modalInstance = $uibModal.open({
			animation: true,
            backdrop  : 'static',
            keyboard  : false,
            size : 'lg',
			templateUrl: '/modalViewOrder',
			controller: 'ModalInstanceViewOrder',
			resolve: {
				'$modalInstance': function() { return function() { return modalInstance; } },
				items: function () {
					return $scope.items;
				}
			}
		});
    };
    $scope.getSellersActive = function() {
        var resj = $http({
             method: 'GET',
             url: '/getSellersActive'
         })
         .success(function(data, status, headers, config){
             $scope.Sellers = data;
            
         })
         .error(function(data, status, headers, config){
             $scope.status = status;
         });
         return resj
     };

    //fin funciones aplicadas al B2B ------------------------------------------------------------------//

});