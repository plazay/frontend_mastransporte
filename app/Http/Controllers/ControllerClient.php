<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;

class ControllerClient extends Controller
{
    public function storeClient(Request $request)
    {
        $data = $request;
        $comission = \App\Client::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'rut' => $data['rut'],
            'platform' => $data['platform'],
         ]);

        return redirect('/home');
    }
    public function updateClient(Request $request)
    {
        //dd($request->all());
        // $validator = Validator::make($request->all(), [
        //     'name' => 'requiere|max:70',
        //     'redirect' => 'required|url'
        // ]);

        // if ($validator->fails()){
        //     return redirect('/home')
        //             ->withInput()
        //             ->withErrors(validator);
        // }

        $client = \App\Client::find($request->id);

        $client->name = $request->name;                          
        $client->email = $request->email;
        $client->rut = $request->rut;                     
        $client->platform = $request->platform;
        $client->countries = $request->countries;

        $client->save();

        return back()->with('flash', 'El cliente fué editado correctamente');
        //var_dump('hahahaha');
    }
    public function deleteClient(Request $request)
    {
        // dd($request->all());
        $client = \App\Client::destroy($request->id);

        Session::flash('message', 'El registro ha sido eliminado');

        return redirect('/home');
    }
    public function getCarriers(Request $request)
    {
        $client = new Client([
            'base_uri' => 'http://localhost:8081/',
        ]);
        
        $response = $client->request('POST', 'asignar/carriers', [
            'form_params' => [
                'id' => $request->id
            ]
        ]);
        
        return $response;
    }
    public function UpdateStatus(Request $request)
    {
        $client = \App\Client::find($request->id);

        $client->tyc = $request->tyc;
        $client->save();
        
        return redirect('/home');
    }

    // public function update(Request $request, $secret)    {
    //     //
    //     $this->validate($request,[ 'name'=>'required', 'platform'=>'required', 'countries'=>'required', 'carriers'=>'required', 'porcentaje'=>'required', 'moneda'=>'required']);
 
    //     client::find($secret)->update($request->all());
    //     return redirect()->route('clientEdit')->with('success','Registro actualizado satisfactoriamente');
 
    // }
}
