<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;
use Mail;

class EmailsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function sendAuthorization(Request $request)
    {
        // $client = \App\Client::find($request->id);
        // $model = new Client::with(['carriers'])->where('id', $request->id)->get();
        $client = new Client([
            'base_uri' => 'http://localhost:8081/',
        ]);
        $response = $client->request('POST', 'asignar/carriers', [
            'form_params' => [
                'id' => $request->id
                
            ]
        ]);
        $response = $response->getBody()->getContents();
        // dd($request);
        // dd(json_decode($response)[0]);
        $data = array(
            'name' => "Mastransporte",
            'carriers' => json_decode($response),
            'id' => $request->id,
            'email' => $request->email,

        );
        $email = $request->email;
        // dd($email);
        Mail::send('emails.authorization', $data, function ($message){
            // dd($request);
            $message->from('yplaza@mastransporte.cl', 'Autorizacion');
            $message->to('yoneiyor-f21@hotmail.com')->subject('Autorizar comision');
        });

        return redirect('/home');
    }
    public function sendTyC(Request $request)
    {
        // guzle para obtener datos de los carriers;
        $auth = \App\Client::find($request->x_id);

        $auth->authorization = $request->x_authorization;
        $auth->save();
        $client = new Client([
            'base_uri' => 'http://localhost:8081/',
        ]);
        $response = $client->request('POST', 'asignar/carriers', [
            'form_params' => [
                'id' => $request->x_id
                
            ]
        ]);
        $response = $response->getBody()->getContents();

        // aqui empieza el proceso de envio el correo
        $data = array(
            'name' => "Mastransporte",
            'carriers' => json_decode($response),
            'id' => $request->x_id,
            
        );
        $email = $request->x_email;
        
        Mail::send('emails.terminos', $data, function ($message) use ($email){
            // dd($request);
            $message->from('yplaza@mastransporte.cl', 'Mastransporte');
            $message->to($email)->subject('Bienvenido');
        });

        return redirect('/home');
    }
    
    public function acceptContract(Request $request)
    {
        // dd($request->tyc);
        $client = \App\Client::find($request->id);

        $client->tyc = $request->tyc;
        $client->save();

        return view('agradecimiento');
    }

}