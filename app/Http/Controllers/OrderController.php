<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\ExcelServiceProviders;
use App\Order;
use App\User;
use App\Notifications;
use App\Notifications\NewOrder;

class OrderController extends Controller
{
    public function UpdateStatusOrder(Request $request)
    {
        // dd($request);
        $order = Order::find($request->id);

        $order->status = $request->status;
        $order->save();
        
        return redirect('/home');
    }
    public function exportOrder(Request $request)
    {
        \Excel::create('Order', function($excel) {
 
            $orders = Order::all();
         
            $excel->sheet('Order', function($sheet) use($orders) {
         
            $sheet->fromArray($orders);
            $sheet->row(1, [
                'ID Cliente', 'ID Tienda', 'N° Pedido', 'Fecha_retiro', 'ID Carrier','Producto', 'Servicio', 'Destino_comuna', 'Peso', 'Destinatario', 'Calle', 'Numero', 'Complemento_direccion',
                'Referencia', 'Alto', 'Ancho', 'Largo', 'Cargo_empresa', 'Monto_cobro_cod', 'Valor_declarado_producto', 'email', 'Celular', 'Entrega_oficina', 'Info_adicional',
                'add_agrupada, Agr_total_piezas', 'Agr_Pieza_numero', 'Empresa', 'Destinatario_secundario', 'Contenido_declarado_producto', 'Cobertura_extendida'
            ]);
            foreach($orders as $index => $order) {
                $sheet->row($index+2, [
                    $order->id_client, $order->id_store, $order->num_order, $order->date, $order->id_carrier,$order->product, $order->service, $order->destination_commune, $order->weight, 
                    $order->addressee, $order->street, $order->number, $order->add_on_address,$order->reference, $order->louder, $order->wide, $order->length, $order->position_company, 
                    $order->code_amount_collected, $order->declared_value_product, $order->email, $order->cell, $order->office_delivery, $order->aditional_info,
                    $order->add_agrupada, $order->add_total_pieces, $order->add_piece_number, $order->company, $order->addressee_secondary, $order->declared_content_product, $order->extended_coverage
                ]); 
            }
         
        });
         
        })->export('xlsx');
    }
   
}
