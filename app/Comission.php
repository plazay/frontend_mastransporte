<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comission extends Model
{
	protected $table = 'comision';
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'countries', 'carriers', 'cost',
    ];
}