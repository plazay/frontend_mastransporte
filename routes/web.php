<?php

use GuzzleHttp\Client;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/prueba', function () {
   
        // $client = new Client([
        //     'base_url' => env('URL_API_LUMEN'),
        // ]);
    
        // $response = $client->request('GET', 'consultarDirecciones');
        // return json_decode( $response->getBody()->getContents());
        // return json_decode($response->getBody()->getContents());

        // -----------------------------------------
        $client = new Client([
            'base_uri' => 'http://localhost:8081/',
        ]);
    
        $response = $client->request('GET', 'consultarDirecciones');
        // return json_decode((string) $response->getBody(), true);

        // ------------------------------------

        // $response = null;
        // $client = new Client(['base_uri' => env('https://orbitvu.samurai.cl/')]);
        // $response = $client->get('info/bab3776a4ebcf29320ba6f11d7e4b058bc082b8f', [
        //     'headers' => [
        //         'Accept' => 'application/json',
        //         // 'api-key' => env('TOKEN_ENVIAME'),
        //         'Content-Type' => 'application/json',
        //     ]
        // ]);

        // $responseData = json_decode($response->getBody());
        // $response = $responseData;
       
        // return $response;

        // ---------------------------------------

    // $http = new GuzzleHttp\Client([
    //     'base_uri' => env('http://localhost:8081/'),
    // ]);

    // // $response = $http->get('http://localhost:8081/consultarDirecciones', []);
    // $response = $http->request('GET', 'http://localhost:8081/consultarDirecciones');
    // // $response = $http->get('http://localhost:8081/consultarDirecciones', []);
    // // $response = $http->get('https://orbitvu.samurai.cl/info/bab3776a4ebcf29320ba6f11d7e4b058bc082b8f', []);

    return json_decode((string) $response->getBody(), true);

});

Route::get('/getEnvio', function () {
    $paramsData = [
        "codigoProducto" => 3, 
        "codigoServicio" => 3,
        "comunaOrigen" => "PUDAHUEL",     
        "numeroTCC" => 18654862,
        "referenciaEnvio" => "T",
        "referenciaEnvio2" => "E",
        "montoCobrar" => 0,
        "eoc"=> 0,
        "nombre"=> "JAZMIN TEST",
        "email"=> "jrincon@chilexpress.cl",
        "celular"=> 97398515,
        "Destinatario"=> [
                "nombre"=> "PRUEBA CARGA TEST",
                "email"=> "jjaimes@chilexpress.cl",
                "celular"=> "97398518"
            ],
        "comuna"=> "RECOLETA",
        "calle"=> "AVENIDA SANTA MARIA",
        "numero"=> 571,
        "complemento"=> "DPTO 2606B JAZMIN Y JONATHAN",
        "DireccionDevolucion"=> [
                "comuna"=> "",
                "calle"=> "",
                "numero"=> "",
                "complemento"=> ""
            ],
        "peso"=> 0.90,
        "alto"=> 1,
        "ancho"=> 1,
        "largo"=> 1
    ];
    // $client = new Client([
    //     'base_uri' => 'http://localhost:8081/',
    //     'form_params' => $paramsData
    // ]);
    $client = new \GuzzleHttp\Client();

    // $response = $client->request('POST', 'generarEnvio');
    // ["form_params"=>$paramsData]
    $response = $client->get('localhost:8081/generarEnvio');

    return json_decode((string) $response->getBody(), true);

});

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@home')->name('home')->middleware('auth');
Route::post('/cliente/estado', 'ControllerClient@UpdateStatus');

//ruta para ver pedidos recibidos
Route::get('/modalViewOrder', 'viewController@modalOrderView');

//ruta para exportar excel de pedidos
Route::get('/export-pedidos', 'OrderController@exportOrder');
Route::get('/updateStatusOrder', 'OrderController@UpdateStatusOrder');



//ruta para enviar correos luego de confirmar comission
Route::get('/emailAuthorization', 'EmailsController@sendAuthorization');
Route::get('/terminosCondiciones', 'EmailsController@sendTyC');
Route::get('/postAcceptContract', 'EmailsController@acceptContract');


Route::post('/saveCarriers', 'CarriersController@save')->middleware('auth');
// Rutas clientes
Route::get('/modalNewClient', 'viewController@modalClientNew')->middleware('auth');
Route::get('/newClient', 'ControllerClient@storeClient')->middleware('auth');

Route::get('/modalClientsEdit', 'viewController@modalClientsEdit')->middleware('auth');
Route::post('/clientUpdate', 'ControllerClient@updateClient')->middleware('auth');

Route::get('/modalClientsDelete', 'viewController@modalClientsDelete')->middleware('auth');
Route::post('/clientDelete', 'ControllerClient@deleteClient')->middleware('auth');

Route::get('/modalClientsReconfig', 'viewController@modalClientsReconfig')->middleware('auth');
Route::get('/ReconfigClient', 'ControllerClient@UpdateStatus')->middleware('auth');

//ruta de conecion a lumen (funcion para ver carriers de clientes)
Route::get('/ver/carriers','ControllerClient@getCarriers');

Route::get('/modalCarriersView', 'viewController@modalCarriersView');

//Rutas Comission
Route::get('/modalNewComission', 'viewController@modalComissionNew')->middleware('auth');
Route::get('/newComission', 'ComissionController@storeComission')->middleware('auth');




// Route::get('/edit', function () {
//     return view('clientesEdit');
// });

