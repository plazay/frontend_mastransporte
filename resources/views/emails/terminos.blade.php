<div style="margin: 2% auto">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="form-group text-center">
					<center><img src="{{asset('/img/samurai.svg')}}" class="img-responsive" width="25%" /></center>
					<h2>Te damos la bienvenida a Mastransporte</h2>
				</div>
			</div>
		</div>
		<br>
		<form id="acceptContract"method="GET" action="{{ url('postAcceptContract') }}" enctype="multipart/form-data" ng-controller="clientesController as cCtrl" ng-init="cCtrl.getCarriers(_aux)">
			<div style="margin: 0 10%">
				<div class="panel panel-default" style="padding: 30px 40px 30px">
					<div class="panel-body no-padding">
						<div class="row" ng-show="loading">
							<div class="col-sm-12">
								<div class="text-center"><i class="fa fa-spinner fa-pulse fa-4x"></i></div>
							</div>
						</div>
						<div class="modal-header">
                            <h3 class="modal-title"> Has escogido los siguientes carriers</h3>
                        </div> 

                                @foreach($carriers as $carr)
                                    <p>
                                        {!! $carr->name !!} - {!! $carr->comission !!}
                                    </p>
                                @endforeach

						<div class="row animated fadeIn" ng-show="!loading">
							<div class="col-sm-12">
								<div class="form group">
									<!-- <object data="https://docs.google.com/document/d/1S0OK-yMaolJvwXs49zDyJcwfj7j7kh7G5AiSgv2hgEg/preview" style="width: 100%; height: 350px;"></object> -->
								</div>
							</div>
						</div>
						<br>
						<div class="row" ng-show="!loading">
							
							<div class="modal-footer">
                                <div class="row" ng-controller="clientesController">
                                    <div class="col-sm-12">
                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                        <input type="hidden" name="id" value="{!! $id !!}">
                                        <input type="hidden" name="tyc" value="1">
                                        <!-- <button class="btn btn-warning btn-sm" ng-click="openAuthorizeClient()">
                                            <i class="btn btn-outline-danger rounded-circle"></i> Aceptar Autorizacion
                                        </button> -->
										<div>
											<a href="https://docs.google.com/document/d/1S0OK-yMaolJvwXs49zDyJcwfj7j7kh7G5AiSgv2hgEg/preview">Ver terminos y condiciones</a>
										</div>
										<div>
											<button type="submit" class="btn btn-primary btn-flat pull-right">Aceptar</button>
										</div>
                                    </div>
                                </div>
                            </div>
							
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>