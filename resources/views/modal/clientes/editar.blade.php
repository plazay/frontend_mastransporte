
<form id="editForm" method="POST" action="{{ url('clientUpdate') }}" class="form-horizontal" ng-controller="clientesController as cCtrl" enctype="multipart/form-data">
  <!-- <input name="_method" type="hidden" value="PUT"/> -->
    <div class="modal-header">
        <h4 class="modal-title"><i class="fa fa-user"></i>&nbsp; Editar Cliente</h4>
        <button type="button" class="close" ng-click="cancel()"><i class="fa fa-times"></i></button>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-12">Nombre*</label>
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-text"><i class="fa fa-user"></i></span>
                            <input type="text" name="name" class="form-control required" ng-model="_aux.name">
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-12" for="nombre">Correo</label>
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                            <input type="text" name="email" class="form-control required" placeholder="email" ng-model="_aux.email">
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-12">Rut*</label>
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-text"><i class="fa fa-address-card"></i></span>
                            <input type="text" name="rut" class="form-control required" placeholder="RUT *" ng-model="_aux.rut">
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-12">Plataforma*</label>
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-text"><i class="fa fa-bullseye"></i></span>
                            <input name="platform" class="form-control required" placeholder="Plataforma" type="text" ng-model="_aux.platform">

                            <!-- <select name="platform" ng-model="persona.medio" ng-options="obj.value as obj.label for obj in persona.canales" required>
                              <option value="">Platform</option>
                            </select> -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-12">Pais</label>
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-text"><i class="fa fa-globe"></i></span>
                            <input type="text" name="countries" class="form-control"  ng-model="_aux.countries">
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
        </div>
        
        <!-- <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="col-sm-12">Vendedor Asignado *</label>
                    <div class="col-sm-12" ng-init="getSellersActive()">
                        
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-home"></i></span>
                            <select name="countries" class="form-control bootstrap required" ng-model="selectedBuyer" ui-select2="{ allowClear: false }" data-placeholder="Seleccione un Vendedor">
                                <option value=""></option>
                                <option ng-repeat="c in countries" ng-selected="[[c.id == selectedBuyer]]" value="[[c.id]]">name: [[c.name]]</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            
        </div> -->

        <!-- /.row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="col-sm-12 small">* Campos Obligatorios</label>
                </div>
            </div>
        </div>
    </div>
    <!--/.modal-body -->
    <div class="modal-footer">
        <div class="row">
            <div class="col-sm-12">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="hidden" name="id" value="[[_aux.id]]">
                <button type="submit" class="btn btn-primary" ng-click="cCtrl.openAlert()">Editar Cliente</button>
                <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
            </div>
        </div>
    </div>
    <!--/.modal-footer -->
</form>
<!-- jQuery Setting Plugins -->
<!-- <script src="{{asset('/js/setting.plugins.js')}}"></script> -->



<!-- <script>alert('dfsgfdsgfdsgfdfgfsgdgsd');</script> -->