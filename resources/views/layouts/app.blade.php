<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" ng-app="app">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="apple-touch-icon" href="favicon/mastransportefavi.ico" />
    <link rel="icon" href="favicon/mastransportefavi.ico" />
    <title>{{ config('app.name', 'Mastransporte') }}</title>
    <!-- Fonts and icons -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
    <!-- Bootstrap core CSS -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script> -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <!-- Modal -->
    <link rel="stylesheet" href="css/w3.css">

</head>
<body>
  <div ng-controller="clientesController">
      <!-- Alert -->
      <div class="container">
          <div class="row justify-content-center">
              <div class="card-body">
                  @if (session('status'))
                      <div class="alert alert-success" role="alert">
                          {{ session('status') }}
                      </div>
                  @endif
              </div>
          </div>
      </div>
    <!-- Header -->
    <nav class="navbar navbar-expand-md fixed-top bg-dark text-white">
      <a class="navbar-brand" href="#">
        <img src="svg/mtlogo.svg">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!-- <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form> -->
        <!-- Left Side Of Navbar -->
        <ul class="navbar-nav mr-auto">

        </ul>
        <!-- Right Side Of Navbar -->
        <ul class="navbar-nav ml-auto">
            <!-- Authentication Links -->
            @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
            @else
                <li class="nav-item dropdown mr-2">
                  <a href="" id="dropdownMenuButton" class="btn btn-outline-info rounded-circle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-h"></i></a>
                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton" ng-controller="comissionController">
                      <a class="dropdown-item" href="#"></a>
                      <a class="dropdown-item" href="" ng-click="openNewComission()"><i class="fa fa-percent"></i> Crear Comision</a>
                      <a class="dropdown-item" href="#"><i class="fa fa-map-marker"></i> Tracking de productos</a>
                      <a class="dropdown-item" href="#"><i class="fa fa-tags"></i> Facturación</a>
                  </div>
                </li>
                <li class="nav-item dropdown mr-2">
                  <div class="container">
                      <div class="row">
                          <div class="dropdown">
                            <a href="" id="dropdownMenu1" class="btn btn-outline-danger rounded" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fa fa-bell-o"></i> 
                                    @if ($count = count($orders))
                                    
                                        <span class="badge">{{ $count }}</span>

                                    @endif

                            </a>
                            <span class="notification"></span>
                              <ul class="dropdown-menu dropdown-menu-right multi-level" role="menu" aria-labelledby="dropdownMenu">
                                  <li class="dropdown-submenu pull-left">
                                    <a  class="dropdown-item" tabindex="-1" href="#">Antier <span class="badge">{{ $count }}</span></a>
                                    <ul class="dropdown-menu">
                                      <li class="dropdown-item"><a class="dropdown-item" href="" ng-click="openViewOrderCHX({{$orders}})"> Chilexpress <span class="badge">{{ $count }}</span></a></li>
                                      <li class="dropdown-item"><a class="dropdown-item" href="" ng-click="openViewOrderCCH({{$orders}})"> Correos Chile</a></li>
                                      <li class="dropdown-item"><a class="dropdown-item" href="" ng-click="openViewOrderDHL({{$orders}})"> DHL</a></li>
                                    </ul>
                                  </li>
                                  <li class="dropdown-submenu pull-left">
                                    <a  class="dropdown-item" tabindex="-1" href="#">Ayer</a>
                                    <ul class="dropdown-menu">
                                    <li class="dropdown-item"><a class="dropdown-item" href="" ng-click="openViewOrderCHX({{$orders}})"> Chilexpress</a></li>
                                    <li class="dropdown-item"><a class="dropdown-item" href="" ng-click="openViewOrderCCH({{$orders}})"> Correos Chile</a></li>
                                    <li class="dropdown-item"><a class="dropdown-item" href="" ng-click="openViewOrderDHL({{$orders}})"> DHL</a></li>             
                                    </ul>
                                  </li>
                                  <li class="dropdown-submenu pull-left">
                                    <a  class="dropdown-item" tabindex="-1" href="#">Hoy</a>
                                    <ul class="dropdown-menu">
                                    <li class="dropdown-item"><a class="dropdown-item" href="" ng-click="openViewOrderCHX({{$orders}})"> Chilexpress</a></li>
                                    <li class="dropdown-item"><a class="dropdown-item" href="" ng-click="openViewOrderCCH({{$orders}})"> Correos Chile</a></li>
                                    <li class="dropdown-item"><a class="dropdown-item" href="" ng-click="openViewOrderDHL({{$orders}})"> DHL</a></li>               
                                    </ul>
                                  </li>             
                                </ul>
                          </div>
                      </div>
                  </div>
                </li>
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle btn btn-sm btn-outline-primary" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            @endguest
        </ul>
      </div>
    </nav>
        <main class="py-4">
            @yield('content')
        </main>
  </div>
    <!-- Footer Page -->
    <footer class="text-muted bg-dark">
        <div class="container">
            <p class="float-right">
            <a href="#top">ir al principio</a>
            </p>
            <p>Mastransporte SpA, &copy; Todos los derechos reservados.</p>
            <p>Nuestro portal web? <a href="www.mastransporte.cl">Visite nuestra pagina</a> or read our 
            <a href="https://docs.google.com/document/d/1mWelGe7XvXEkIkXPilkoWuXi1OOJk6uwQ_0gEdJrk3E/edit">leer nuestros terminos y condiciones</a>.</p>
        </div>
    </footer>
    <!-- Scripts -->
    <script src="js/bootstrap.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.2/angular.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('/angularController/mainAppAdmin.js')}}"></script>
    <script src="{{ asset('/angularController/clientesController.js')}}"></script>
    <script src="{{ asset('/angularController/comissionController.js')}}"></script>
    <script src="{{ asset('/angularController/modalController.js')}}"></script>
    <script src="{{ asset('/bower_components/angular-bootstrap4/dist/ui-bootstrap.js')}}"></script>
    <script src="{{ asset('/bower_components/angular-bootstrap4/dist/ui-bootstrap-tpls.js')}}"></script>

</body>
</html>