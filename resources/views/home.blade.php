@extends('layouts.app')
@section('content')
<main role="main">
  
    <div class="container">
      <form class="form-inline mt-4 justify-content-between">
        <input class="form-control mr-sm-2" type="text" placeholder="Search" style aria-label="Search" />
        <!-- <input class="btn btn-primary" type="button" onclick="document.getElementById('id03').style.display='block'" style name="send" value="lista" /> -->
        <div ng-controller="clientesController">

          <!-- <button class="btn btn-success btn-sm" ng-click="openNewComission()">
              <i class="fa fa-percent"></i> Crear Comision
          </button> -->

          <!-- <button class="btn btn-warning btn-sm" ng-click="openNewClient()">
              <i class="fa fa-user-plus"></i> Nuevo Cliente
          </button> -->

          <button class="btn btn-primary btn-sm" type="button" onclick="document.getElementById('id01').style.display='block'" style name="send">
            <i class="fa fa-user-plus"></i> Nuevo Cliente
          </button>
        </div>
      </form>
      <br>
      
      <div class="row" ng-controller="clientesController as cCtrl">
        @foreach($clients as $client)
        <div class="col-md-4">
          <div class="card mb-4 box-shadow shadow p-3 mb-5 bg-white rounded">
            <div class="container">
              <div class="row">
                <div class="col-md-12 ">
                  <div class="card">
                    <!-- <div class="card-header" data-background-color="blue"> -->
                    <div class="card mb-4 box-shadow p-3 mb-2 bg-secondary text-white">
                      <center><h4 class="title">{{ $client->name }}</h4></center>
                      <p class="category">Rut: {{ $client->rut }}</p>
                    </div>

                    <div class="card-content table-responsive">
                        <p class="text-primary container">ID</p>
                        <!-- <p class="no-margin container" ng-model="cli.client" ng-init="cli.client = {{ $client->id }};">{{ $client->id }}</p> -->
                        <p class="no-margin container">{{ $client->id }}</p>
                    </div>
                    <div class="card-content table-responsive">
                          <p class="text-primary container">Secret</p>
                          <p class="no-margin container">{{ $client->secret }}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="card-body">
              <!-- <p class="card-text">Informacion de la API.</p> -->
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">

                  <!-- <form action="{{ url('oauth/token') }}" method="get">
                  <input type="submit" class="btn btn-sm btn-outline-secondary" name="send" value="ver"/>
                  </form> -->

                  <!-- <input class="btn btn-sm btn-outline-secondary" type="button" onclick="document.getElementById('id02').style.display='block'" style name="send" value="Generar acceso" /> -->
                  @if ($client->tyc == 1)
                  <a href="" ng-click="openReconfigClient({{$client}})" class="btn btn-sm btn-outline-secondary"> Reconfigurar comision</a>
                  @else
                  <!-- <a href="" ng-click="openViewCarriers({{$client->id}})" class="btn btn-outline-success rounded-circle"><i class="fa fa-percent"></i></a> -->
                  <a href="" ng-click="openViewCarriers({{$client}})" class="btn btn-sm btn-success"> Confirmar comision</a>
                  @endif
                </div>
                <div class="d-flex justify-content-between">

                  <a href="" ng-click="openEditClient({{$client}})" class="btn btn-outline-info rounded-circle"><i class="fa fa-edit"></i></a>
                  <a href="" ng-click="openDeleteClient({{$client}})" class="btn btn-outline-danger rounded-circle"><i class="fa fa-trash"></i></a>
                  
                  <!-- <a href="/editar/comision" class="btn btn-outline-success rounded-circle"><i class="fa fa-percent"></i></a> -->
                  
                  <!-- <a href="/ver/carriers?id={{$client->id}}" class="btn btn-outline-dark rounded-circle"><i class="fa fa-truck"></i></a> -->

                  

                </div>



                <!-- <small class="text-muted">{{ $client->carriers }}</small> -->
              </div>
            </div>
          </div>
        </div>
        @endforeach
      </div>

      
      <!-- include('modal.clientes.eliminar') -->


      <!-- Modal del client -->
      <div id="id01" class="w3-modal w3-animate-opacity" style="z-index:10000">
        <div class="w3-modal-content" style="border-radius: 15px;">
          <div class="w3-container">
            <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-display-topright" style="border-top-right-radius: 15px; border-bottom-left-radius: 15px;">&times;</span><br><br>
            <div class="jumbotron text-center">
              <div class="row">
                <div class="col-md-12">
                  <div class="card">
                    <!-- <div class="card-header" data-background-color="blue"> -->
                    <div class="card mb-4 box-shadow p-3 mb-2 bg-secondary text-white">
                      <h4 class="title">Nuevo Cliente</h4>
                        <p class="category"></p>
                    </div>
                    <h1 class="jumbotron-heading">Datos del cliente</h1>
                    <form class="form-group" action="{{ url('oauth/clients') }}" method="POST">
                      <div class="d-flex justify-content-around">
                      <p>
                        <input type="text" name="name" placeholder="Nombre" />
                      </p>
                      <p>
                        <input type="text" name="email" placeholder="Ingrese su email" />
                      </p>
                      <p>
                        <input type="text" name="redirect" placeholder="URL de tienda" />
                      </p>
                      </div>
                      <div class="d-flex justify-content-center">
                      <p>
                        <input type="text" name="rut" placeholder="Ingrese su RUT" />
                      </p>
                      </div>
                      <p>
                        <select class="custom-select container" name="platform">
                            <option selected="true" disabled="disabled">Plataforma</option>
                            @foreach($platforms as $platform)
                            <option name="platform">{{$platform->name}}</option>
                            @endforeach
                        </select>
                      </p>
                      <p>
                        <select class="custom-select container" name="countries">
                            <option selected="true" disabled="disabled">Pais</option>
                            @foreach($countries as $countrie)
                            <option name="countries">{{$countrie->nameCountries}}</option>
                            @endforeach
                        </select>
                      </p>
                      <p>
                        <input type="hidden" name="status" value="1">
                      </p>
                      <p>
                        <input class="btn btn-success my-2" type="submit" name="send" value="Crear" />
                        <!-- <script>
                        alert("Usuario creado exitosamente");
                        windows.history.back();
                        </script> -->
                        <!-- <input class="btn btn-primary my-2" type="button" onclick="document.getElementById('id03').style.display='block'" style name="send"  value="siguiente" /> -->
                        <!-- <button  onclick="document.getElementById('id01').style.display='block'" class="btn btn-default ">Ver clientes</button><hr> -->
                      </p>
                      {{ csrf_field() }}
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div id="id03" class="w3-modal" style="z-index:10000">
        <div class="w3-modal-content" style="border-radius: 15px;">
          <div class="w3-container">
            <span onclick="document.getElementById('id03').style.display='none'" class="w3-button w3-display-topright" style="border-top-right-radius: 15px; border-bottom-left-radius: 15px;">&times;</span><br><br>
            <section class="jumbotron text-center">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-12">
                    <div class="card">
                      <div class="card-header" data-background-color="blue">
                        <h4 class="title">Detalles de Clientes</h4>
                          <p class="category"></p>
                      </div>
                      <div class="card-content table-responsive">
                        <table class="table">
                          <thead class="text-primary">
                            <tr>
                              <th>ID</th>
                              <th>Name</th>
                              <th>Redirect</th>
                              <th>Platform</th>
                              <th>Countries</th>
                              <th>Carriers</th>

                            </tr>
                          </thead>
                          <tbody>
                            @foreach($clients as $client)
                            <tr>
                              <td>{{ $client->id }}</td>
                              <td>{{ $client->name }}</td>
                              <td>{{ $client->redirect }}</td>
                              <td>{{ $client->platform }}</td>
                              <td>{{ $client->countries }}</td>
                              <td>{{ $client->cariiers }}</td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>


    </div>
 
</main>
@endsection