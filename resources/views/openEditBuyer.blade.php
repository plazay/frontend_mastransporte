<form id="editForm" method="post" action="/editBuyer" class="form-horizontal" ng-controller="globalController" enctype="multipart/form-data">
    <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()"><i class="fa fa-times"></i></button>
        <h4 class="modal-title"><i class="fa fa-user"></i>&nbsp; Editar Cliente</h4>
    </div>
    <div class="modal-body" ng-controller="clientesController">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-12">Rut Cliente *</label>
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-credit-card-alt"></i></span>
                            <input type="text" name="rut" class="form-control required" ng-model="_aux.rut" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-12" for="nombre">Nombre *</label>
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="text" name="nombre" class="form-control required" placeholder="Nombre *" ng-model="_aux.nombre">
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-12">Apellido *</label>
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="text" name="paterno" class="form-control required" placeholder="Apellido *" ng-model="_aux.apellidoPaterno">
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-12">Email *</label>
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            <input name="email" class="form-control required" placeholder="Email" type="text" ng-model="_aux.email">
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-12">Teléfono</label>
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-phone-square"></i></span>
                            <input type="text" name="fono" class="form-control" placeholder="Teléfono" ng-model="_aux.home_phone" numbers-only maxlength="15">
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-12">Celular</label>
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-phone-square"></i></span>
                            <input type="text" name="celular" class="form-control" placeholder="Celular" ng-model="_aux.cell_phone" numbers-only maxlength="15">
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="col-sm-12">Empresa *</label>
                    <div class="col-sm-12" ng-init="getCompanyActiveBack()">
                        <!-- Ya existe -->
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-home"></i></span>
                            <select name="empresa" class="form-control bootstrap" ng-class="{'required':!showEmpresa}" ng-model="selectedEmpresa" ui-select2="{ allowClear: false }" data-placeholder="Seleccione una opción">
                                <option value=""></option>
                                <option ng-repeat="e in Empresas" ng-selected="[[e.id == selectedEmpresa]]" value="[[e.id]]">Rut: [[e.rut]] - [[e.razonSocial]]</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="col-sm-12">Vendedor Asignado *</label>
                    <div class="col-sm-12" ng-init="getSellersActive()">
                        <!-- Ya existe -->
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-home"></i></span>
                            <select name="seller" class="form-control bootstrap required" ng-model="selectedBuyer" ui-select2="{ allowClear: false }" data-placeholder="Seleccione un Vendedor">
                                <option value=""></option>
                                <option ng-repeat="s in Sellers" ng-selected="[[s.id == selectedBuyer]]" value="[[s.id]]">Nombre: [[s.nombre]]</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="col-sm-12 small">* Campos Obligatorios</label>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!--/.modal-body -->
    <div class="modal-footer">
        <div class="row">
            <div class="col-sm-12">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="hidden" name="idCliente" value="[[_aux.id]]">
                <button type="submit" class="btn btn-primary">Editar Cliente</button>
                <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
            </div>
        </div>
    </div>
    <!--/.modal-footer -->
</form>
<!-- jQuery Setting Plugins -->
<script src="{{asset('/js/setting.plugins.js')}}"></script>