<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Clientes</title>
		<link href="css/material-dashboard.css?v=1.2.0" rel="stylesheet" />
		<!-- Fonts and icons -->
		<link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
		
		<!-- Bootstrap core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!-- Modal -->
		<link rel="stylesheet" href="css/w3.css">
	</head>
	<body>
		<header>
				<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
				<a>
						<i class="material-icons">local_shipping</i>
						<strong>Mastransporte</strong>
				</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarCollapse">
						<ul class="navbar-nav mr-auto">
							<li class="nav-item active">
								<a class="nav-link" href="{{ url('/home') }}">Home <span class="sr-only">(current)</span></a>
							</li>

						</ul>
					</div>
				</nav>
		</header>

		<main role="main">
			<section class="jumbotron text-center">
								<div class="container-fluid">
									<div class="row">
										<div class="col-md-12">
											<div class="card">
												<div class="card-header" data-background-color="blue">
													<h4 class="title">Clientes</h4>
														<p class="category"></p>
												</div>
												<div class="card-content table-responsive">
													<table class="table">
														<thead class="text-primary">
															<tr>
																<th>ID</th>
																<th>Name</th>
																<th>Redirect</th>
																<th>Secret</th>
																<th>Platform</th>
																<th>Countries</th>
																<th>Carriers</th>

															</tr>
														</thead>
														<tbody>
															@foreach($clients as $client)
															<tr>
																<td>{{ $client->id }}</td>
																<td>{{ $client->name }}</td>
																<td>{{ $client->redirect }}</td>
																<td>{{ $client->secret }}</td>
																<td>{{ $client->platform }}</td>
																<td>{{ $client->countries }}</td>
																<td>{{ $client->cariiers }}</td>
															</tr>
															@endforeach
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</main>	
	</body>
</html>