<form id="newForm" method="get" action="{{ url('oauth/clients') }}" class="form-horizontal" ng-controller="comissionController" enctype="multipart/form-data">
  <!-- <input name="_method" type="hidden" value="PUT"/> -->
    <div class="modal-header">
        <h4 class="modal-title"><i class="fa fa-user-plus"></i>&nbsp; Crear Cliente</h4>
        <button type="button" class="close" ng-click="cancel()"><i class="fa fa-times"></i></button>
    </div>
    <div class="modal-body" ng-controller="clientesController">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-12">Nombre*</label>
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-text"><i class="fa fa-user"></i></span>
                            <input type="text" name="name" class="form-control required" >
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-12" for="nombre">Correo</label>
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                            <input type="text" name="email" class="form-control required" placeholder="ingrese su mail" >
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-12">Rut*</label>
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-text"><i class="fa fa-address-card"></i></span>
                            <input type="text" name="rut" class="form-control required" placeholder="RUT *" >
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-12">Plataforma*</label>
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-text"><i class="fa fa-bullseye"></i></span>
                            <input name="platform" class="form-control required" placeholder="Plataforma" type="text" >

                            <!-- <select name="platform" ng-model="persona.medio" ng-options="obj.value as obj.label for obj in persona.canales" required>
                              <option value="">Platform</option>
                            </select> -->

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-12">Pais</label>
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-text"><i class="fa fa-globe"></i></span>
                            <input type="text" name="countries" class="form-control"  >
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-12">URL</label>
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-text"><i class="fa fa-truck"></i></span>
                            <input type="text" name="redirect" class="form-control"  >
                        </div>
                    </div>
                </div>
            </div>

        <!-- <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="col-sm-12">Vendedor Asignado *</label>
                    <div class="col-sm-12" ng-init="getSellersActive()">
                        
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-home"></i></span>
                            <select name="countries" class="form-control bootstrap required" ng-model="selectedBuyer" ui-select2="{ allowClear: false }" data-placeholder="Seleccione un Vendedor">
                                <option value=""></option>
                                <option ng-repeat="c in countries" ng-selected="[[c.id == selectedBuyer]]" value="[[c.id]]">name: [[c.name]]</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            
        </div> -->

        <!-- /.row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="col-sm-12 small">* Campos Obligatorios</label>
                </div>
            </div>
        </div>
    </div>
    <!--/.modal-body -->
    <div class="modal-footer">
        <div class="row">
            <div class="col-sm-12">
                <!-- <input type="hidden" name="monto_total" value="500">
                <input type="hidden" name="id" value="[[_aux.id]]"> -->
s                <button type="submit" class="btn btn-primary">Crear</button>
                <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
            </div>
        </div>
    </div>
    <!--/.modal-footer -->
</form>
<!-- jQuery Setting Plugins -->
<!-- <script src="{{asset('/js/setting.plugins.js')}}"></script> -->