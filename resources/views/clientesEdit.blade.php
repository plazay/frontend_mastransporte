@extends('layouts.app')
@section('content')
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>home</title>
    <!--  Material Dashboard CSS    -->
    <!-- <link href="css/material-dashboard.css" rel="stylesheet" /> -->
    <!-- Fonts and icons -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/album.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

		<!-- Modal -->
		<link rel="stylesheet" href="css/w3.css">
  </head>
  
  <div class="container">
    <div class="row justify-content-center">
      <div class="card-body">
          @if (session('status'))
              <div class="alert alert-success" role="alert">
                  {{ session('status') }}
              </div>
          @endif
      </div>  
    </div>
  </div>
  <body>
		<div class="jumbotron text-center">
			<div class="row">
			<div class="col-md-12">
				<div class="card">
				<!-- <div class="card-header" data-background-color="blue"> -->
				<div class="card mb-4 box-shadow p-3 mb-2 bg-secondary text-white">
					<center><h4 class="title ">{{ $client->name }}</h4></center>
					<p class="category"></p>
				</div>
				<h1 class="jumbotron-heading">Datos del cliente</h1>
				<form action="{{ url('client') }}/{{ $client->id}}" method="POST">
					{{ csrf_field() }}
					{{ method_field('PUT') }}
					<p>
					<input type="text" name="name" placeholder="Nombre" value="{{ $client->name}}"/>
					</p>
					<p>
					<input type="text" name="redirect" placeholder="URL de redirección" value="{{ $client->redirect}}"/>
					</p>
					<p>
					<select class="custom-select container" name="platform">
						<option selected="true" disabled="disabled">{{ $client->platform}}</option>
						@foreach($platforms as $platform)
						<option name="platform">{{$platform->name}}</option>
						@endforeach
					</select>
					</p>
					<p>
					<select class="custom-select container" name="countries">
						<option selected="true" disabled="disabled">{{ $client->countries}}</option>
						@foreach($countries as $countrie)
						<option name="countries">{{$countrie->nameCountries}}</option>
						@endforeach
					</select>
					</p>
					<div class="card-content table-responsive">
					<h1 class="jumbotron-heading">Selecione carriers y comision</h1>
					<table class="table container">
						<tbody>
							<tr>
								<td></td>
								<td>%</td>
								<td>$</td>
							</tr>
							<tr>
							<td><input type="checkbox" name="carriers" aria-label="Checkbox for following text input" value="{{ $client->carriers}}"/>Chilexpress</td>
							<td><input type="text" name="porcentaje" Class="" value="{{ $client->porcentaje}}"/></td>
							<td><input type="text" name="moneda" Class="" value="{{ $client->moneda}}"/></td>
							</tr>
							<tr>
							<td><input type="checkbox" name="carriers" aria-label="Checkbox for following text input" value="{{ $client->carriers}}"/>CorreosChile</td>
							<td><input type="text" name="porcentaje" Class="" value="{{ $client->porcentaje}}"/></td>
							<td><input type="text" name="moneda" Class="" value="{{ $client->moneda}}"/></td>
							</tr>
							<!-- <input class="btn btn-primary my-2" type="submit" name="send" value="conectar" /> -->
							<tr>
							<td><input type="checkbox" name="carriers" aria-label="Checkbox for following text input" value="{{ $client->carriers}}"/>DHL</td>
							<td><input type="text" name="porcentaje" Class=""/></td>
							<td><input type="text" name="moneda" Class=""/></td>
							</tr>
							<tr>
							<td><input type="checkbox" name="carriers" aria-label="Checkbox for following text input" value="{{ $client->carriers}}"/>Starken</td>
							<td><input type="text" name="porcentaje" Class=""/></td>
							<td><input type="text" name="moneda" Class=""/></td>
							</tr>
							<tr>
							<td><input type="checkbox" name="carriers" aria-label="Checkbox for following text input" value="{{ $client->carriers}}"/>Bluexpress</td>
							<td><input type="text" name="porcentaje" Class=""/></td>
							<td><input type="text" name="moneda" Class=""/></td>
							</tr>
						</tbody>
					</table>
					</div> 
					
					<p>
					<input class="btn btn-primary my-2" type="submit" name="send" value="Guardar" />
					<!-- <script>
					alert("Usuario creado exitosamente");
					windows.history.back();
					</script> -->
					<!-- <input class="btn btn-primary my-2" type="button" onclick="document.getElementById('id03').style.display='block'" style name="send"  value="siguiente" /> -->
					<!-- <button  onclick="document.getElementById('id01').style.display='block'" class="btn btn-default ">Ver clientes</button><hr> -->
					</p>
				</form>
				</div>
			</div>
			</div>
		</div>
		</div>
	</div>
	</div>


   
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <!-- <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery-slim.min.js"></script>')</script> -->
    <script src="assets/js/vendor/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="assets/js/vendor/holder.min.js"></script>
  </body>
</html>
@endsection