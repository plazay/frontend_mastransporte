<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ComissionController extends Controller
{
    public function storeComission(Request $request)
    {
        $data = $request;
        $comission = \App\Comission::create([
            'code' => $data['code'],
            'countries' => $data['countries'],
            'carriers' => $data['carriers'],
            'cost' => $data['cost'],
         ]);

        return redirect('/home');
    }
    public function index()
    {
        $comision = \App\Comission::all();
        return view('comission', compact('comissions'));
    }
    public function updateComission(Request $request)
    {
        //dd($request->all());
        // $validator = Validator::make($request->all(), [
        //     'name' => 'requiere|max:70',
        //     'redirect' => 'required|url'
        // ]);

        // if ($validator->fails()){
        //     return redirect('/home')
        //             ->withInput()
        //             ->withErrors(validator);
        // }

    $comission = \App\Comission::find($request->id);

    $comission->code = $request->code;                          
    $comission->countries = $request->countries;
    $comission->carriers = $request->carriers;                     
    $comission->cost = $request->cost;
    // $comission->monto_total = $request->monto_total;
    // $comission->porcentaje = 10;
    // $comission->moneda = 1;
    $comission->save();

    Session::flash('message', 'El registro ha sido modificado');

     return redirect('/home');
    //var_dump('hahahaha');
    }
    public function deleteComission(Request $request)
    {
        // dd($request->all());
        $comission = \App\Comission::destroy($request->id);

        Session::flash('message', 'El registro ha sido eliminado');

        return redirect('/home');
    }

    // public function update(Request $request, $secret)    {
    //     //
    //     $this->validate($request,[ 'name'=>'required', 'platform'=>'required', 'countries'=>'required', 'carriers'=>'required', 'porcentaje'=>'required', 'moneda'=>'required']);
 
    //     comission::find($secret)->update($request->all());
    //     return redirect()->route('comissionEdit')->with('success','Registro actualizado satisfactoriamente');
 
    // }
}
