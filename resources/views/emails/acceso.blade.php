<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Acceso</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div class="jumbotron text-center">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="card">
                        <div class="card mb-4 box-shadow p-3 mb-2 bg-info text-white">
                          
                            <p class="category"></p>
                        </div>
                        
                        <h1 class="jumbotron-heading">¡Para nosotros es un placer!</h1>
                        {!! $name !!}

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
</body>
</html>