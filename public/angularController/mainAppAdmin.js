var app = angular.module("app", ['ui.bootstrap','ui.bootstrap.modal'])
.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

//xeditable theme
// app.run(['editableOptions', 'editableThemes', function(editableOptions, editableThemes) {
//     editableThemes.bs3.inputClass = 'input-sm';
//     editableThemes.bs3.buttonsClass = 'btn-sm';
//     editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
// }]);

// app.run(function(amMoment) {
//     amMoment.changeLocale('es');
// });

//Currency Percentage
app.filter('percentage', ['$filter', function ($filter) {
    return function (input, decimals) {
        return $filter('number')(input, decimals) + '%';
    };
}]);

//HTML entities
app.filter('unsafe', function($sce){
    return function(val){
        return $sce.trustAsHtml(val);
    };
});

//Primer caracter mayuscula
app.filter("ucwords", function () {
    return function (input){
        if(input) { //when input is defined the apply filter
           input = input.toLowerCase().replace(/\b[a-z]/g, function(letter) {
              return letter.toUpperCase();
           });
        }
        return input; 
    }    
});

app.filter('capitalize', function() {
    return function(input, all) {
      var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
      return (!!input) ? input.replace(reg, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    }
});

//datepicker
// app.directive("datepicker", function () {
//   return {
//     restrict: "A",
//     require: "ngModel",
//     link: function (scope, elem, attrs, ngModelCtrl) {
//       var updateModel = function (dateText) {
//         scope.$apply(function () {
//           ngModelCtrl.$setViewValue(dateText);
//         });
//       };
//       var options = {
//           format: "yyyy-mm-dd",
//           language: "es",
//           orientation: "bottom left",
//           autoclose: true,
//           startDate: '0d',
//           endDate: '+90d',
//         onSelect: function (dateText) {
//           updateModel(dateText);
//         }
//       };
//       elem.datepicker(options);
//       elem.datepicker("setDate", new Date());
//     }
//   }
//  });

//Message Toastr
// app.config(function(toastrConfig) {
//     angular.extend(toastrConfig, {
//         allowHtml: true,
//         closeButton: true,
//         closeHtml: '<button>&times;</button>',
//         extendedTimeOut: 1000,
//         iconClasses: {
//             error: 'toast-error',
//             info: 'toast-info',
//             success: 'toast-success',
//             warning: 'toast-warning'
//         },  
//         messageClass: 'toast-message',
//         onHidden: null,
//         onShown: null,
//         onTap: null,
//         progressBar: true,
//         tapToDismiss: true,
//         templates: {
//             toast: 'directives/toast/toast.html',
//             progressbar: 'directives/progressbar/progressbar.html'
//         },
//         timeOut: 10000,
//         titleClass: 'toast-title',
//         toastClass: 'toast'
//     });
// });

//Flow Image Upload
// app.config(['flowFactoryProvider', function (flowFactoryProvider) {
//     flowFactoryProvider.defaults = {
//         target: '',
//         permanentErrors: [500, 501],
//         maxChunkRetries: 1,
//         chunkRetryInterval: 5000,
//         simultaneousUploads: 1
//     };
// }]);

//Languaje App DataTable
// app.run(function(DTDefaultOptions) {
//     DTDefaultOptions.setLanguage({
//         sUrl: '/bower_components/datatables/i18n/Spanish.json'
//     });
// });

//Input type numeric
app.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^+0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }            
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

//Plugins Input mask format
// app.directive('inputMask', function(){
//     return {
//         restrict: 'A',
//         link: function(scope, el, attrs){
//             $(el).inputmask(scope.$eval(attrs.inputMask));
//         }
//     };
// });