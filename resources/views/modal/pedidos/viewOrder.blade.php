<form id="viewOrder" method="get" action="{{ url('download/order') }}" class="form-horizontal" ng-controller="clientesController"enctype="multipart/form-data">
    <div class="modal-header">
        <h4 class="modal-title"><i class="fa fa-cubes"></i>&nbsp; Pedidos Chilexpress</h4>
        <button type="button" class="close" ng-click="cancel()"><i class="fa fa-times"></i></button>
    </div>

  <div class="col-md-12 mr-3">
    <!-- <div class="card box-shadow shadow p-3 mb-5 bg-white rounded"> -->
      <div class="modal-header" data-background-color="blue">
        <h4 class="title">Detalles de pedidos</h4>
        <a href="/export-pedidos" class="close"><i class="fa fa-file-excel-o"></i></a>
          <p class="category"></p>
      </div>
      <div class="table-responsive">
        <table class="table table-hover">
          <thead class="text">
            <tr>
              <th>ID Cliente</th>
              <th>Numero Pedido</th>
              <th>Fecha Retiro</th>
              <th>Carrier</th>
              <th>Comuna Destino</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat="order in _aux">
              <td>[[order.client_id]]</td>
              <td>[[order.num_order]]</td>
              <td>[[order.date]]</td>
              <td>[[order.id_carrier]]</td>
              <td>[[order.destination_commune]]</td>
              <td><input type="checkbox" name="status" aria-label="Checkbox for following text input"></td>
            </tr>
          </tbody>
        </table>
      </div>
    <!-- </div> -->
</form>
<!-- enviar el checkbox y hacer que cuando sea true se eliminen de la vista el numero de notificaciones
se debe enviar el request del checkbox desde aqui -->
    <!--/.modal-body -->
    <div class="modal-footer">
        <div class="row">
          <form method="get" action="{{ url('updateStatusOrder') }}" class="form-horizontal" enctype="multipart/form-data">
            <div class="col-sm-12">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="hidden" name="id" value="[[_aux[0].id]]">
                <input type="hidden" name="status" value="1">
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
          </form>
        <button type="button" class="btn btn-default" ng-click="cancel()">Cerrar</button>
        </div>
    </div>
    <!--/.modal-footer -->  
