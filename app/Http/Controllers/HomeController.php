<?php

namespace App\Http\Controllers;

use App\Order;
use App\Events\OrderShipped;
use App\Notifications\NewOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function home()
    {
        $clients = \App\Client::where('user_id', Auth::user()->id)->get();
        // $clients = \App\Client::where('user_id',null)->get();
        // $clients = \App\Client::all();       
        $countries = \App\Countrie::all();
        $platforms = \App\Platform::all();
        $orders = \App\Order::all();

        // event(new OrderShipped($orders));

        return view('home', compact('clients', 'countries', 'platforms', 'orders'));
    }

}