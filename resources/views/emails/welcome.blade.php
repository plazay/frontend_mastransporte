<div style="margin: 2% auto">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="form-group text-center">
					<center><img src="{{asset('/img/samurai.svg')}}" class="img-responsive" width="25%" /></center>
					<h2>Te damos la bienvenida a Mastransporte</h2>
				</div>
			</div>
		</div>
		<br>
		<form action="/postAcceptContract" method="post" enctype="multipart/form-data">
			<div style="margin: 0 10%">
				<div class="panel panel-default" style="padding: 30px 40px 30px">
					<div class="panel-body no-padding">
						<div class="row" ng-show="loading">
							<div class="col-sm-12">
								<div class="text-center"><i class="fa fa-spinner fa-pulse fa-4x"></i></div>
							</div>
						</div>
						<div class="row animated fadeIn" ng-show="!loading">
							<div class="col-sm-12">
								<div class="form group">
									<object data="https://docs.google.com/document/d/1S0OK-yMaolJvwXs49zDyJcwfj7j7kh7G5AiSgv2hgEg/preview" style="width: 100%; height: 350px;"></object>
								</div>
							</div>
						</div>
						<br>
						<div class="row" ng-show="!loading">
							<div class="col-sm-12">
								<button type="submit" class="btn btn-primary btn-flat pull-right">Aceptar</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>