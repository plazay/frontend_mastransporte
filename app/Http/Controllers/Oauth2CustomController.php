<?php
namespace App\Http\Controllers;
// namespace Laravel\Passport\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laravel\Passport\ClientRepository;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;
// use App\ClientCustomRepository;
// use App\GuzzleHttp\Client;


// namespace App\Http\Controllers;

// use Illuminate\Http\Request;

class Oauth2CustomController extends Controller
// class Oauth2CustomController extends ClientController
{
    /**
     * The client repository instance.
     *
     * @var \Laravel\Passport\ClientRepository
     */
    protected $clients;

    /**
     * The validation factory implementation.
     *
     * @var \Illuminate\Contracts\Validation\Factory
     */
    protected $validation;

    /**
     * Create a client controller instance.
     *
     * @param  \Laravel\Passport\ClientRepository  $clients
     * @param  \Illuminate\Contracts\Validation\Factory  $validation
     * @return void
     */
    public function __construct(ClientRepository $clients, ValidationFactory $validation)
    // public function __construct(ClientRepository $clients, ValidationFactory $validation)
    {
        $this->clients = $clients;
        $this->validation = $validation;
    }

    /**
     * Store a new client.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validation->make($request->all(), [
            'name' => 'required|max:255',
            'redirect' => 'required|url',
        ])->validate();

        return $this->clients->create(
            $request->user()->getKey(), $request->name, $request->redirect,false, false, $request->platform, $request->countries, $request->carriers 
        )->makeVisible('secret');
        

        // var_dump($request->all());
        // die();
        // $http = new \GuzzleHttp\Client;
        
        // // $response = $http->post( url('oauth/clients'), [
        // $response = $http->post('http://localhost:8000/oauth/clients', [
        // // $response = $http->post('http://your-app.com/oauth/token', [
        //     /*'form_params' => [
        //         'grant_type' => 'password',
        //         'client_id' => 'client-id',
        //         'client_secret' => 'client-secret',
        //         'username' => 'taylor@laravel.com',
        //         'password' => 'my-password',
        //         'scope' => '',
        //     ],*/
        //     'form_params' => $request->all(),
        // ]);

        // return json_decode((string) $response->getBody(), true);
    }
}
